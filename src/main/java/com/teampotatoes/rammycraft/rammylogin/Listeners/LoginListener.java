package com.teampotatoes.rammycraft.rammylogin.Listeners;

import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import net.kyori.adventure.text.Component;
import com.teampotatoes.rammycraft.rammylogin.RammyLogin;
import org.geysermc.floodgate.api.FloodgateApi;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;

public class LoginListener implements Listener
{
    private RammyLogin instance;

    public LoginListener(RammyLogin instance)
    {
        this.instance = instance;
    }

    @EventHandler
    public void onPlayerJoin(AsyncPlayerPreLoginEvent event)
    {
        String name = event.getName();
        UUID uuid = event.getUniqueId();
        List<Player> playerList = new ArrayList<>(Bukkit.getOnlinePlayers());
        FloodgateApi floodgateApi = FloodgateApi.getInstance();
        for(Player player : playerList){
            if (player.getName().equals(name)){
                final Component textComponent = Component.text(instance.getConfig().getString("kickMessage")).color(NamedTextColor.RED);
                event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, textComponent);
                break;
            }
        }
        if (!Pattern.matches("^[a-zA-Z0-9_]*$", name))
        {
            final Component textComponent = Component.text(instance.getConfig().getString("illegalMessage")).color(NamedTextColor.RED);
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, textComponent);
            return;
        }
        else if (floodgateApi != null && ((!floodgateApi.isFloodgatePlayer(uuid) && name.startsWith("BR_")) || (floodgateApi.isFloodgatePlayer(uuid) && name.startsWith("BR_BR_")))) {
            final Component textComponent = Component.text("\"BR_\" name is reserved for system use, please use a different name!").color(NamedTextColor.RED);
            event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, textComponent);
            return;
        }
    }
}
