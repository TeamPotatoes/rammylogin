package com.teampotatoes.rammycraft.rammylogin.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.util.StringUtil;
import com.teampotatoes.rammycraft.rammylogin.RammyLogin;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class RlCommand implements CommandExecutor
{
    private final RammyLogin instance;

    public RlCommand(RammyLogin instance)
    {
        this.instance = instance;
    }

    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, String[] args)
    {
        if (args.length > 0)
        {
            if (args[0].equalsIgnoreCase("reload"))
            {
                return new Rl_ReloadCommand(instance).onCommand(sender, command, label, args);
            }
        }
        sender.sendMessage(ChatColor.YELLOW + "Rammy Login " + instance.getDescription().getVersion());
        sender.sendMessage("");
        sender.sendMessage("/" + label + " reload - Reload rammylogin configuration.");
        return true;
    }

    public static TabCompleter tabCompleter = (commandSender, command, s, args) ->
    {
        List<String> completions = new ArrayList<>();
        if (args.length == 1){
            completions.add("reload");
        }

        return StringUtil.copyPartialMatches(args[args.length - 1], completions, new ArrayList<>());
    };
}
