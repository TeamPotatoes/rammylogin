package com.teampotatoes.rammycraft.rammylogin.Commands;

import com.teampotatoes.rammycraft.rammylogin.RammyLogin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Rl_ReloadCommand implements CommandExecutor
{
    private RammyLogin instance;

    public Rl_ReloadCommand(RammyLogin instance){
        this.instance = instance;
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args){
        instance.reloadConfig();
        sender.sendMessage(ChatColor.YELLOW + "Configuration successfully reloaded.");
        return true;
    }
}
