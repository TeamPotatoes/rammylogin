package com.teampotatoes.rammycraft.rammylogin;

import com.teampotatoes.rammycraft.rammylogin.Commands.RlCommand;
import com.teampotatoes.rammycraft.rammylogin.Listeners.LoginListener;
import org.bukkit.plugin.java.JavaPlugin;

public class RammyLogin extends JavaPlugin
{

    @Override
    public void onEnable()
    {
        this.saveDefaultConfig();

        getServer().getPluginManager().registerEvents(new LoginListener(this), this);

        this.getCommand("rl").setExecutor(new RlCommand(this));
    }

    @Override
    public void onDisable()
    {

    }
}
